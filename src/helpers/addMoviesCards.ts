import { Imovie } from "../interface/movie";
import movieCard from "../components/movieCard";

export default function addMovies (movies: Array<Imovie>): void {
  const movieContainer: HTMLElement = document.getElementById("film-container");
  let tempContainer = '';
  movies.forEach(movie => {
    tempContainer += movieCard(movie);
  })
  movieContainer.innerHTML = movieContainer.innerHTML + tempContainer;
}
