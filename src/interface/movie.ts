export interface Imovie {
  id: number,
  backdrop_path: string,
  overview: string,
  poster_path: string,
  release_date: string,
  title: string,
}