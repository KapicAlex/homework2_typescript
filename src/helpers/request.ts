import { API_URL, API_KEY } from "../config/config";

export default async function requestApi(endpoint: string, params: Record<string, unknown>, method: string): Promise<void> {
  let allParams = "";
  let fullUrl = "";
  let subUrl = "";

  if(params['movie_id']) {
    for (const [key, value] of Object.entries(params)) {
      if (key === "movie_id") {
        subUrl = endpoint + value + "?api_key=";
        continue;
      } 
      allParams += `&${key}=${value}`
    }
    fullUrl = API_URL + subUrl + API_KEY + allParams;
  }else{
    for (const [key, value] of Object.entries(params)) {
      allParams += `&${key}=${value}`
    }
    fullUrl = API_URL + endpoint + API_KEY + allParams;
  }
  
  const response = await fetch(fullUrl, {method: method});
  return await response.json();
}