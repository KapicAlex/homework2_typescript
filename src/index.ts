import movieDB from "./api/api"
import randomMovie from "./helpers/addRandomMovie";
import addMovies from "./helpers/addMoviesCards";
import movieMapper from "./helpers/mapper";
import { Imovie } from "./interface/interfaces"
import { addListeners, makeFavourite } from "./helpers/handlerClick";
import addFavouriteCards from "./helpers/addFavouriteCards";


export async function render(): Promise<void> {
    
    const buttonsCategory: NodeListOf<Element> = document.querySelectorAll('input[name="btnradio"]');
    const buttonSearch: HTMLElement = document.getElementById('submit');
    const buttonLoadMore: HTMLElement = document.getElementById('load-more');
    const movieContainer: HTMLElement = document.getElementById("film-container");
    
    let likes: NodeListOf<Element>;
    let currentPage = 1;
    let currentCategory = "popular";
    let currentQuery ='';

    async function renderRandomMovie(page: number = 1): Promise<void> {
      let movieData: any;
      movieData = await movieDB.getPopular({page});
      const moviesList: Array<Imovie> = movieMapper(movieData.results);
      randomMovie(moviesList);
    }

    renderRandomMovie();

    async function renderMovies(page: number = 1, category: string, query?: string | undefined, movie_id?: number | undefined ): Promise<void> {
      let movieData: any;

      switch (category) {
        case "search":
          movieData = await movieDB.search({page, query})
          break;
        case "popular":
          movieData = await movieDB.getPopular({page})
          break;
        case "top_rated":
          movieData = await movieDB.getTopRated({page})
          break;
        case "upcoming":
          movieData = await movieDB.getUpcoming({page})
          break;
        case "details":
          movieData = await movieDB.getDetails({movie_id})
          break;
        default:
          break;
      }

      const moviesList: Array<Imovie> = movieMapper(movieData.results);

      addMovies(moviesList);
      
      let likes = document.querySelectorAll('.bi-heart-fill');
        
      makeFavourite (likes);
    }

    await renderMovies(currentPage, currentCategory);
    await addFavouriteCards(likes);
    likes = document.querySelectorAll('.bi-heart-fill');

    addListeners (buttonsCategory, buttonSearch, buttonLoadMore, movieContainer,
                    currentPage, currentCategory, currentQuery, renderMovies);
}
