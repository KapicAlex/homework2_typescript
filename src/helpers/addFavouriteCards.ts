import movieDB from "../api/api"
import movieMapper from "./mapper";
import { Imovie } from "../interface/movie";
import favouriteCard from "../components/favourite";

export default async function addFavouriteCards (): Promise<void> {
  
  const favouriteContainer: HTMLElement = document.getElementById("favorite-movies");
  let tempContainer = '';
  const tempData: any = [];
  let movieData: any;
  const storageRaw: string | null = localStorage.getItem('favourite');
  
  if(storageRaw) {
    const storage: Record<string, unknown> = JSON.parse(storageRaw);
    for (const movie_id in storage){
      movieData = await movieDB.getDetails({movie_id});
      tempData.push(movieData);
    }
    const moviesList: Array<Imovie> = movieMapper(tempData);
    moviesList.forEach(movie => {
      tempContainer += favouriteCard(movie);
    });
    favouriteContainer.innerHTML = tempContainer;
  }

}