import { route as ROUTE } from "./routes";
import requestApi from "../helpers/request";
import { IurlSearchParams, IurlPopularParams, IurlTopRated, IurlUpcoming, IurlDetails } from "../interface/interfaces";

class MovieDB {

  async search(params: IurlSearchParams): Promise<void> {
    return await requestApi(ROUTE.url.search, params, "GET");
  }

  async getPopular(params: IurlPopularParams): Promise<void> {
    return await requestApi(ROUTE.url.popular, params, "GET");
  }

  async getTopRated(params: IurlTopRated): Promise<void> {
    return await requestApi(ROUTE.url.topRated, params, "GET");
  }

  async getUpcoming(params: IurlUpcoming): Promise<void> {
    return await requestApi(ROUTE.url.upcoming, params, "GET");
  }

  async getDetails(params: IurlDetails): Promise<void> {
    return await requestApi(ROUTE.url.details, params, "GET");
  }
}

const movieDB = new MovieDB();
export default movieDB;