interface IurlSearchParams {
  language?: string,
  query: string | undefined,
  page: number,
  include_adult?: boolean,
  region?: string,
  year?: number,
  primary_release_year?: number,
}

interface IurlPopularParams {
  language?: string,
  page: number,
  region?: string,
}

interface IurlTopRated {
  language?: string,
  page: number,
  region?: string,
}

interface IurlUpcoming {
  language?: string,
  page: number,
  region?: string,
}

interface IurlDetails {
  movie_id: number | undefined | string,
  language?: string,
  append_to_response?: string,
}

export {
  IurlSearchParams,
  IurlPopularParams,
  IurlTopRated,
  IurlUpcoming,
  IurlDetails,
} 