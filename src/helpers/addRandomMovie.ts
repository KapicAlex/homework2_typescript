import { Imovie } from "../interface/movie";
import random from "../components/random";
import { IMG_URL } from "../config/config";

export default function randomMovie (movies: Array<Imovie>): void {
  const randomNum: number = Math.floor(Math.random() * (movies.length + 1));
  const randomMovieSection: HTMLElement = document.getElementById("random-movie");
  randomMovieSection.style.backgroundImage = `url(${IMG_URL + movies[randomNum].backdrop_path}`
  randomMovieSection.innerHTML = random(movies[randomNum]);
}

