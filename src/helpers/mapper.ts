import { Imovie } from "../interface/movie";

export default function movieMapper (movieData: []): Array<Imovie> {
  const moviesList: Array<Imovie> = [];
  for (const movieDefault of movieData) {
    const movie: Imovie = select(movieDefault, 'id', 'backdrop_path', 'overview', 'poster_path', 'release_date', 'title');
    moviesList.push(movie);
  }
  return moviesList;
}

function select<T, K extends keyof T>(movieDefault: T, ...keys: K[]): Imovie {
  const movie: any = {};
  keys.forEach(el => {
    movie[el] = movieDefault[el];
  });
  return movie;
}