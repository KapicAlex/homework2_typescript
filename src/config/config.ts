export const API_URL = process.env.API_URL;
export const API_KEY = process.env.API_KEY;
export const IMG_URL = "https://image.tmdb.org/t/p/original/";
