export const route = {
  url: {
    search: "search/movie?api_key=",
    popular: "movie/popular?api_key=",
    topRated: "movie/top_rated?api_key=",
    upcoming: "movie/upcoming?api_key=",
    details: "movie/",
  }
}