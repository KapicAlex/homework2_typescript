export function isFavourite (id: number): boolean {
  const storageRaw: string | null = localStorage.getItem('favourite');
  if(storageRaw) {
    const storage: Record<string, unknown> = JSON.parse(storageRaw);
    if(storage[id]) {
      return true;
    }
    return false;
  }
  return false;
}

export function delFavourite (id: number | string): void {
  const storageRaw:string = localStorage.getItem('favourite');
  const storage: Record<string, unknown> = JSON.parse(storageRaw);
  delete storage[id];
  localStorage.setItem('favourite', JSON.stringify(storage));

  const hearts = document.querySelectorAll(`[data-id]`);
  hearts.forEach(heart => {
    if(heart.dataset.id === id){
      heart.setAttribute("style", "fill: #ff000078");
    }
  })
}

export function addFavourite (id: number): void {
  const storageRaw:string | null = localStorage.getItem('favourite');
  let storage: Record<string, unknown> = {};
  if(storageRaw) {
    storage = JSON.parse(storageRaw);
    storage[id] = true;
  }else{
    storage[id] = true;
  }
  localStorage.setItem('favourite', JSON.stringify(storage));

  const hearts = document.querySelectorAll(`[data-id]`);
  hearts.forEach(heart => {
    if(heart.dataset.id === id){
      heart.setAttribute("style", "fill: red");
    }
  })
}