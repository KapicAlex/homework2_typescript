import { addFavourite, delFavourite, isFavourite } from "./addFavourite";
import addFavouriteCards from "./addFavouriteCards";

export function addListeners (categorys, search, loadMore, movieContainer,
                        currentPage, currentCategory, currentQuery, renderMovies) {
  categorys.forEach((btn) => {
    btn.addEventListener('click', (e: Event) => {
      const category: string = btn.id;
      movieContainer.innerHTML = '';
      renderMovies(...[,], category);
      currentCategory = category;
    })
  })

  search.addEventListener('click', () => {
    const query = document.getElementById('search').value ?? '';
    movieContainer.innerHTML = '';
    currentCategory = "search";
    renderMovies(...[,], currentCategory, query);
    currentQuery = query;
    document.getElementById('search').value = '';
  })

  loadMore.addEventListener('click', () => {
    currentPage++;
    if (!currentQuery) {
      renderMovies(currentPage, currentCategory);
    }else{
      renderMovies(currentPage, currentCategory, currentQuery);
    }
  })
}

export function makeFavourite (hearts): void {
  hearts.forEach((heart) => {

    heart.addEventListener('click', () => {
      const id: number = heart.dataset.id;

      if(isFavourite(id)){
        delFavourite(id);
      }else{
        addFavourite(id);
      }

      addFavouriteCards();
    })
  });
}